

**Program description**

The purpose of this script is, from a series of images, to perform a color calibration, selecting a certain area that we want to define as a perfect white.


There is in the Example folder a series of 4 figures, where we want to measure the green/red proportion of the leaf.
Unfortunately, the yellow light affects the colors.
So we put a piece of white paper, which is more or less at the same place on all the pictures.

The goal is to modify the photos so that the piece of paper is a perfect white (255,255,255), and we can then make our measurements on the colors of the leaves.

This script has the following principle: First we specify a rectangular area that will be, on all photos, in the white area. Then this area will be used to calibrate the photos.

**Install Python**


Download the right version of the python idle on the official website : https://www.python.org/downloads/

**Installed the PIL library**


- Linux :
	$ python3 -m pip install Pillow

- Windows : 
	$ py -m pip install pillow


**Execution of the program**
The requirements are satisfied, we can now execute the program.

1. The images for the white balance must be put in the same folder

2. Launch the python IDE and open the file whiteBalance.py

3. A window opens to select the photos you want to calibrate.

4. A 2nd window opens to select the folder where the new photos will be saved

5. A third window opens, and you can use the mouse to select a rectangle where you want it to be.

Once we have selected an area, we press the T key to generate the test images.
These images are used to check that the selected area will be in the white area, on all the photos.
If the selected rectangle is outside the area, then we can start this step again, by reselecting a rectangle, and pressing T again.

When the rectangle is valid, press the enter key. 

6. The new photos are then created in the folder, and the python window closes
