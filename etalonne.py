from tkinter import filedialog as fd
from tkinter import messagebox as mb
import tkinter as tk

from PIL import Image, ImageDraw
from PIL import ImageTk

from os import path

def ask_file():
	filepaths = fd.askopenfilenames(title="Select Pictures", filetypes=(("Images",["*.JPG","*.jpg","*.JPEG","*.jpeg"]), ("all files","*.*")))
	if len(filepaths) < 1:
		mb.showerror(message="No files selected")
		exit()
	return filepaths

def ask_dir():
	output_path = fd.askdirectory(title="Select Output Directory")
	if output_path == ():
		mb.showerror(message="No folder selected")
		exit()	
	return output_path



def check_same_proprety(names):
	imgRef = Image.open(names[0])

	size = imgRef.size
	form = imgRef.format
	mode = imgRef.mode
	
	for k in names[1:]:
		img = Image.open(k)
		if img.size != size:
			msg = f'The {k} image has a different size than the {names[0]} image.\n{img.size} != {size}'
			print(msg)
			exit()
		if img.format != form:
			msg = f'The {k} image has a different format than the {names[0]} image.\n{img.format} != {form}'
			print(msg)
			exit()
		if img.mode != mode:
			msg = f'The {k} image has a different mode than the {names[0]} image.\n{img.mode} != {mode}'
			print(msg)
			exit()



def end(Ev=None):
	root.quit()


def test(Ev=None):

	xa = int(SQUAREA[0]/RATIO)
	xb = int(SQUAREA[1]/RATIO)
	ya = int(SQUAREB[0]/RATIO)
	yb = int(SQUAREB[1]/RATIO)

	print(f"Test with ({xa},{xb}),({ya},{yb})")

	for i in range(len(NAMES_PICS)):
		img  = Image.open(COMPLETE_NAMES_PICS[i])
		img1 = ImageDraw.Draw(img)
		img1.rectangle([(xa,xb),(ya,yb)], fill =None, outline ="red",width=5)
		out_file = path.join(OUTPUT_DIR, NAMES_PICS[i][0]+"-redSquare"+NAMES_PICS[i][1])
		img.save(out_file)
		print("\t",out_file)




def Press(event=None):
	global SQUAREA, SQUAREB
	SQUAREA = (event.x,event.y)
	SQUAREB = (event.x,event.y)
	canvas.coords("zone",SQUAREA[0],SQUAREA[1],SQUAREB[0],SQUAREB[1])

def Drag(event=None):
	global SQUAREB
	SQUAREB = (event.x,event.y)
	canvas.coords("zone",SQUAREA[0],SQUAREA[1],SQUAREB[0],SQUAREB[1])


def colorSquare(im, square):
	# image_array = np.array( im )
	xmin,xmax = min(square[0],square[2]), max(square[0],square[2])
	ymin,ymax = min(square[1],square[3]), max(square[1],square[3])

	R,V,B = [],[],[]
	for x in range(xmin,xmax):
		for y in range(ymin,ymax):
			pxl = im.getpixel((x,y))
			R.append(pxl[0])
			V.append(pxl[1])
			B.append(pxl[2])

	r = sum(R)//len(R)
	v = sum(V)//len(V)
	b = sum(B)//len(B)
	return (r,v,b)


def balance(inputFile,outFile, goal, square):
	print(inputFile,"->",outFile)
	imageIn = Image.open(inputFile)

	col = colorSquare(imageIn, square)

	print("\tActual :",col)
	print("\tGoal   :",goal)
	fact = []
	for i in range(3):
		fact.append(goal[i]/col[i])

	print("\tFact   :",fact)

	mat = [fact[0],       0,       0, 0,
		         0, fact[1],       0, 0,
		         0,       0, fact[2], 0]

	outImage = imageIn.convert(mode="RGB", matrix = mat)
	outImage.save(outFile)

	
######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
######################################################################

root = tk.Tk()
######################################################################
# get filenames
COMPLETE_NAMES_PICS = ask_file()
PATH_PICT = path.split(COMPLETE_NAMES_PICS[0])[0]

print("Dir inputs Files :",PATH_PICT)
print("Nb inputs files  :",len(COMPLETE_NAMES_PICS))

NAMES_PICS = []
for k in COMPLETE_NAMES_PICS:
	t = path.split(k)
	if t[0] != PATH_PICT:
		print("all images must be in the same folder")
		exit()

	NAMES_PICS.append(path.splitext(t[1]))

# check if all pict has same proprety
check_same_proprety(COMPLETE_NAMES_PICS)
######################################################################
# get output directory
OUTPUT_DIR  = ask_dir()
print("Output Folder    :",OUTPUT_DIR)
print()

######################################################################
# get SQUARE
img = Image.open(COMPLETE_NAMES_PICS[0])

WIDTH  = img.width
HEIGHT = img.height
RATIO  = 1000/WIDTH

SQUAREA = (0,0)
SQUAREB = (0,0)

img = img.resize((int(WIDTH*RATIO), int(HEIGHT*RATIO)))
img = ImageTk.PhotoImage(master=root, image=img) 

canvas = tk.Canvas(root,width=int(WIDTH*RATIO), height=int(HEIGHT*RATIO))
canvas.pack()

canvas.create_image(0,0,image=img,anchor="nw")

canvas.create_rectangle(SQUAREA[0],SQUAREA[1],SQUAREB[0],SQUAREB[1],outline="red",tags="zone")

root.bind_all('<Return>',end)
root.bind_all('<T>',test)
root.bind_all('<t>',test)

root.bind("<Button-1>",Press)
root.bind('<B1-Motion>',Drag)

root.mainloop()

SQUARE = [
	int(SQUAREA[0]/RATIO),
	int(SQUAREA[1]/RATIO),
	int(SQUAREB[0]/RATIO),
	int(SQUAREB[1]/RATIO),
]
print("Zone :",SQUARE)
print()
######################################################################
# white balance

GOAL = (255,255,255)

print("White balance :")

for i in range(len(COMPLETE_NAMES_PICS)):
	out = path.join(OUTPUT_DIR, NAMES_PICS[i][0]+"-whiteBalance"+NAMES_PICS[i][1])
	balance(COMPLETE_NAMES_PICS[i], out, goal=GOAL, square=SQUARE)
	print()



